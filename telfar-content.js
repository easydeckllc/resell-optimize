let step = 0;
let account = [];
let final_checkout = false;

function script(step) {
    if(step === 0) {
        logData('Clicking add to cart button.');
        $('#AddToCart').click();

        setTimeout(function () {
            step = 1;

            init(step);
        }, 3000);
    } else if(step === 1) {
        logData('Clicking view cart button.');
        // After add to cart, click view cart button

        $('.btn--view-cart').click();
    } else if(step === 2) {
        logData('Clicking checkout button.');
        // Click checkbox button

        $('[name="checkout"]').click();
    } else if(step === 3) {
        logData('Waiting for captcha to be solved');
        // Wait for ReCaptcha to be solved...

        let taskPostInfo = {
            'type': 'RecaptchaV3TaskProxyless',
            'websiteURL': 'https://shop.telfar.net/checkpoint',
            'websiteKey': '6LcCR2cUAAAAANS1Gpq_mDIJ2pQuJphsSQaUEuc9',
            'minScore': '0.1'
        };

        // todo: make faster by providing callback url, and then spam ping a local easyhuzl.com url for solution

        let createTaskPostData = {"clientKey": "e18a8c960c4c472d77ac6661fb67d278", task: taskPostInfo};

        $.ajax('https://api.capmonster.cloud/createTask', {
            data: JSON.stringify(createTaskPostData),
            type: 'POST',
            headers: {
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Methods': 'HEAD, GET, POST, PUT, PATCH, DELETE, OPTIONS'
            },
            success: function (e) {
                let createJson = JSON.parse(e);
                let task_id = createJson.taskId;

                if(task_id > 0) {
                    let intervalCount = 0;
                    let interval = setInterval(function (e) {
                        intervalCount++;
                        $('.checkpoint__message').text('Solve Captcha to continue (Count: ' + intervalCount + ') | Time Used: ' + (3*intervalCount) + ' seconds');

                        let getTaskResultData = {"clientKey": "e18a8c960c4c472d77ac6661fb67d278", taskId: task_id};

                        $.ajax('https://api.capmonster.cloud/getTaskResult', {
                            data: JSON.stringify(getTaskResultData),
                            type: 'POST',
                            headers: {
                                'Access-Control-Allow-Origin': '*',
                                'Access-Control-Allow-Methods': 'HEAD, GET, POST, PUT, PATCH, DELETE, OPTIONS'
                            },
                            success: function (e) {
                                let resultJson = JSON.parse(e);
                                let solution = resultJson.solution;

                                if (solution) {
                                    clearInterval(interval);
                                    $('.checkpoint__message').text('SOLVED');

                                    $('g-recaptcha-response').val(solution);
                                    $('[type="submit"]').click();
                                }
                            }
                        });
                    }, 3000);
                }
            }
        });

    } else if(step === 4) {
        logData('Entering in shipping information.');

        // Enter in shipping information...

        $('#checkout_email').val(account.shipping_email);
        $('#checkout_shipping_address_first_name').val(account.shipping_firstname);
        $('#checkout_shipping_address_last_name').val(account.shipping_lastname);
        $('#checkout_shipping_address_company').val(account.shipping_company);
        $('#checkout_shipping_address_address1').val(account.shipping_address_1);
        $('#checkout_shipping_address_address2').val(account.shipping_address_2);
        $('#checkout_shipping_address_city').val(account.shipping_city);
        $('#checkout_shipping_address_province').val('MI');
        $('#checkout_shipping_address_zip').val(account.shipping_zipcode);
        $('#checkout_shipping_address_phone').val(account.shipping_phone_number);

        setTimeout(function () {
            $('#checkout_buyer_accepts_marketing').click();
            $('#continue_button').click();

            setTimeout(function () {
                step = 5;

                //init(step);
            }, 4000);
        }, 1000);
    } else if(step === 5) {
        logData('Confirming shipping.');

        // Confirm shipping and move on!

        $('#continue_button').click();

        setTimeout(function () {
            step = 6;

            //init(step);
        }, 4000);
    } else if(step === 6) {
        if(final_checkout) {
            logData('Clicking final pay button...');

            //$('#continue_button').click();
        } else {
            logData('Not clicking final pay button...');
            logData('Waiting for cc information to be filled out');
        }

        // required disable:
        // chrome://flags/
        // Disable site isolation
        // disabled

        let cc_number_field = $('.card-fields-iframe').contents().find('#number');
        let cc_name_field = $('.card-fields-iframe').contents().find('#name');
        let cc_expiry_field = $('.card-fields-iframe').contents().find('#expiry');
        let cc_verification_value_field = $('.card-fields-iframe').contents().find('#verification_value');

        cc_number_field.val(account.card_number);
        cc_name_field.val(account.shipping_firstname + ' ' + account.shipping_lastname);
        cc_expiry_field.val(account.expire_date);
        cc_verification_value_field.val(account.ccv);


        $('#checkout_different_billing_address_false').click();

        $('#continue_button').click();
    }
}

function init(step) { // Init is used to stop the run if stopped and we cannot refresh!
    chrome.storage.local.get(['run'], function(result) {
        if (result.run == true || Object.keys(result).length == 0) {
            script(step);
        }
    });
}

function startUp(account_id) {
    logData('Acc ID: ' + account_id);

    $.post('https://resell.clients.easydeck.net/api/telfar/ping', {account_id: account_id, extension_version: chrome.runtime.getManifest().version}, function(data) {
        if(!data.success) {
            alert('unable to start telfar script... ' + data.msg);
            return;
        }
        chrome.storage.local.set({'account': data.account});

        account = data.account;

        if($('.btn--view-cart').length > 0) {
            step = 1;
        } else if($('[name="checkout"]').length > 0) {
            step = 2;
        } else if(window.location.href.indexOf('checkpoint?') > -1) { // Captcha
            step = 3;
        } else if($('#checkout_email').length > 0) { // Shipping info
            step = 4;
        } else if($('.card-fields-iframe').length > 0) {
            step = 6;
        } else if(window.location.href.indexOf('step=shipping_method') > -1) { // Shipping method step=shipping_method
            step = 5;
        }

        logData('Step: ' + step);

        init(step);
    });
}

function logData(msg) {
    var d = new Date();

    console.log('[TELFAR_RESELL - ' + d.toLocaleString() + '] - ' + msg);

    //$.post('https://resell.clients.easydeck.net/api/ping', {message: msg, url: window.location.href, extension_version: chrome.runtime.getManifest().version}, function() {
    // console.log('sent...')
    //});
}

function pingActivate() {
    logData('Pining to see if drop is active');

    $.get('https://resell.clients.easydeck.net/api/telfar/ping/active', {extension_version: chrome.runtime.getManifest().version}, function(data) {
        if(data.success) {
            if(data.active) {
                logData('Active reloading the page....');
                location.reload();
            } else {
                logData('Not active yet.');
            }
        } else {
            logData('No success : ' + data.error);
        }
    });
}

chrome.storage.local.get(['run', 'telfar_id', 'telfar_final_checkout'], function(result) {
	console.log('===============');
    console.log("Telfar (telfar-content.js)");
    console.log('===============');

    final_checkout = result.telfar_final_checkout;

    console.log(final_checkout);

  	if(result.run == true || Object.keys(result).length == 0) {
        startUp(result.telfar_id);
  	} else {
        logData('Run not enabled.');
  	}
});