let step = 0;
let sizeHasBeenSelected = false;
let ccv = 0;
let sizes = '';
let final_submit = false;
let payment_box_shown = false;

// faster lookup: 
// https://www.nike.com/launch/t/daybreak-pure-platinum?size=9.5&productId=b30f7025-43e4-5572-974d-118fa79982df

function script(step) {
    if(step === 3) { // .checkout-summary-section-content
        logData('Clicking submit order (todo: not yet coded in)');

        let checkout_summary = $('.checkout-summary-section-content')[0].classList;
        if(typeof checkout_summary !== 'undefined' && !checkout_summary.contains('checkout-section-expandable-closed')) {
            logData('Seeing submit order button, putting up an alert');

            // todo: change the alert based on if clicked or not

            var txt3 = document.createElement("div");  // Create with DOM
            txt3.innerHTML = "See submit order btn.";
            txt3.style.background = 'red';
            txt3.style.position = 'absolute'
            txt3.style.width = '100px'
            txt3.style.height = '100px'
            txt3.style.top = '0%'
            txt3.style.left = '0%'
            txt3.style.zIndex = '1000'

            $('body').append(txt3);

            let orderBtn = $('.checkout-summary-section-content .ncss-btn-primary-dark');
            if(orderBtn) {
                if(final_submit) {
                    logData('Clicking on final submit order button');
                    orderBtn.click(); // todo:
                } else {
                    logData('At final submit but not clicking it.');
                }
            }
        } else {
            setTimeout(function () {
                if($('.checkout-summary-section-content .ncss-btn-primary-dark').length === 0) {
                    logData('Reloading because payment button not active after 10s...');

                    // location.reload(); // todo: reenable later
                }
            }, 10000);
        }
    } else if(step === 2) { // .payment-section
        logData('In payment section');

        if(payment_box_shown) {
            let payment_component = $('.payment-component > div')[0];
            if(typeof payment_component !== 'undefined' && !payment_component.classList.contains('checkout-section-expandable-closed')) {
                $('#checkout-sections > header').append('CCV: ' + ccv);

                logData('Clicking on payment method');
                $('.payment-section .ncss-label').click(); // alt: .payment-icn // $('.payment-section .payment-icn');

                setTimeout(function () {
                    logData('Entering CCV into frame');
                    let ccv_field = $('[title="creditCardIframeForm"]').contents().find('#cvNumber');
                    ccv_field.val(ccv);

                    logData('Triggering the on blur event on ccv field.');

                    // blur is very important to get the next button to work correctly.
                    var event = new Event('blur', {bubbles: true});
                    let ccv_field_js_obj = ccv_field.get(0);
                    ccv_field_js_obj.focus();
                    ccv_field_js_obj.dispatchEvent(event);

                    setTimeout(function () {
                        logData('Clicking Save & Continue button');

                        $('.payment-section [data-qa="save-button"]').click();

                        setTimeout(function () {
                            step = 3;

                            init(step);
                        }, 2000);
                    }, 2000);
                }, 2000);
            } else {
                // Shoe out of stock, get another one

                logData('Payment section not auto expanding, loading another size.');

                //pingActivate(); // TODO: ?
            }
        } else {
            logData('Waiting for payment popup (5s)...')

            setTimeout(function () { // Let it load the box every time
                let payment_component = $('.payment-component > div');
                if(payment_component.length > 0 && !payment_component[0].classList.contains('checkout-section-expandable-closed')) {
                    logData('Payment box NOT visible');
                } else {
                    payment_box_shown = true;
                    logData('Payment box visible');
                }

                init();
            }, 4000);
        }
    } else if (step === 1 && $('.size').length > 0) {
        let total_active_sizes_left = $('.size button:not([disabled])').length;

        logData('Number of sizes left: ' + total_active_sizes_left);

        let found_shoe_size = false;

        let active_size_elements = $('.size button:not([disabled])').shuffle().each(function (index) {
            let sizeRegex = /(M (\d*\.?\d+)) |^(\d*\.?\d+)/g;

            let currentSizeMatch = sizeRegex.exec($(this).text());
            let currentSize = 0;

            if(currentSizeMatch[2] !== undefined) { // "M 13 / F 15"
                currentSize = currentSizeMatch[2];
            } else { // "13"
                currentSize = currentSizeMatch[0];
            }

            if (sizes.indexOf(currentSize) !== -1 && !found_shoe_size) {
                found_shoe_size = true; // make sure no more looping
                let size_element = $(this);
                let size = size_element.text();

                logData('Chosen size: ' + size);
                logData('Clicking size button');

                // console.log(size_element[0]);
                // console.log(size_element.find('button'));

                size_element.find('button').click();
                size_element.click();

                setTimeout(function () {
                    logData('Clicking on checkout button');
                    $('.ncss-btn-primary-dark').click();

                    setTimeout(function () {
                        step = 2;

                        init(step);
                    }, 5000);
                }, 1000);
            }
        });
    } else if(!sizeHasBeenSelected && $('.size').length > 0) {
        sizeHasBeenSelected = true;

        setTimeout(function() {
            step = 1;

            init(step);
        }, 2000);
    } else {
        logData('Waiting for drop...');

        setTimeout(function() {
            init(step);

            pingActivate();
        }, 4000);
    }
}

function init(step) { // Init is used to stop the run if stopped and we cannot refresh!
    if(step === 0 && window.location.href.indexOf('?size=') > -1) { // skip to step 2 since we automatically get to the payment screen.
        step = 2;
        sizeHasBeenSelected = true;
    } else if(step === 0 && window.location.href.indexOf('productId=') > -1) { // skip to step 2 since we automatically get to the payment screen.
        step = 2; // we will have to resubmit and get a new size from server tho
        sizeHasBeenSelected = true;
    }

    chrome.storage.local.get(['run'], function(result) {
        if (result.run == true || Object.keys(result).length == 0) {
            script(step);
        }
    });
}

function startUp() {
    logData('Checking if active...');

    let name_element = document.getElementsByClassName('test-name');
    if(name_element.length === 0) {
        logData('Name not shown, waiting 5s and rechecking...');

        setTimeout(function() {
            startUp();
        }, 5000);
    } else {
        logData('Performing initial ping to get ccv and sizes...');

        let name = document.getElementsByClassName('test-name')[0].innerText;
        let url = window.location.href.replace(/\?size.*/g,'');

        $.post('https://resell.clients.easydeck.net/api/ping', {name: name, drop_url: url, active_sizes: activeSizes(), extension_version: chrome.runtime.getManifest().version}, function(data) {
            chrome.storage.local.set({'ccv': data.data.account.ccv});
            chrome.storage.local.set({'sizes': data.data.sizes});
            chrome.storage.local.set({'final_submit': data.data.final_submit});

            ccv = data.data.account.ccv;
            sizes = data.data.sizes;
            final_submit = data.data.final_submit;
            
            init(step);
        });
    }
}

function logData(msg) {
    var d = new Date();

    console.log('[NIKE_RESELL - ' + d.toLocaleString() + '] - ' + msg);

    //$.post('https://resell.clients.easydeck.net/api/ping', {message: msg, url: window.location.href, extension_version: chrome.runtime.getManifest().version}, function() {
    // console.log('sent...')
    //});
}

function activeSizes() {
    let sizes = [];

    $('.size button:not([disabled])').each(function(e) {
        let sizeRegex = /(M (\d*\.?\d+)) |^(\d*\.?\d+)/g;
        let currentSizeMatch = sizeRegex.exec($(this).text());
        let currentSize = 0;

        if(currentSizeMatch[2] !== undefined) { // "M 13 / F 15"
            currentSize = currentSizeMatch[2];
        } else { // "13"
            currentSize = currentSizeMatch[0];
        }

        sizes.push(currentSize);
    });

    return sizes;
}

function pingActivate() {
    logData('Pining to see if shoe is active');

    let name = document.getElementsByClassName('test-name')[0].innerText;
    let url = window.location.href.replace(/\?size.*/g,'');

    $.get('https://resell.clients.easydeck.net/api/ping/active', {name: name, drop_url: url, active_sizes: activeSizes(), extension_version: chrome.runtime.getManifest().version}, function(data) {
        if(data.success) {
            if (data.product_url !== '') {
                logData('Got URL, going to reload to product url: ' + data.product_url);
                window.location.href = data.product_url;
            } else if (data.active) {
                logData('Going to reload for drop..');
                location.reload();
            }
        } else {
            logData('No success : ' + data.error);
        }
    });
}

$.fn.shuffle = function(){
    for(var j, x, i = this.length; i; j = Math.floor(Math.random() * i), x = this[--i], this[i] = this[j], this[j] = x);
    return this;
};

function randomNumber(max_number) {
    return Math.floor(Math.random() * (max_number + 1));
}

chrome.storage.local.get(['run'], function(result) {
	console.log('===============');
    console.log("Nike (nike-content.js)");
    console.log('===============');

  	if(result.run == true || Object.keys(result).length == 0) {
        startUp();
  	} else {
        logData('Run not enabled.');
  	}
});