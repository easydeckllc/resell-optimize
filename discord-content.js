
// PNY GeForce RTX 3060 Ti 8GB Uprising Dual Fan Graphics Card
// ASUS TUF Gaming NVIDIA GeForce RTX 3070 OC Edition Graphics Card- PCIe 4.0, 8GB GDDR6, HDMI 2.1 , DisplayPort 1.4a, Dual Ball Fan Bearings
// MSI Gaming GeForce RTX 3080 10GB GDRR6X 320-Bit HDMI/DP Nvlink Torx Fan 3 Ampere Architecture OC Graphics Card (RTX 3080 VENTUS 3X 10G OC)

// DO NOT EDIT BELOW THIS, EDIT USING EXTENSION POPUP
let wanted_products = 'RTX 3060|GeForce RTX 3070|GeForce RTX 3080|PlayStation 5 Digital Edition|PlayStation 5 Console|5700 xt';
let max_price = 800;
let submit_order = true;
let delay_time = 2000;
// DO NOT EDIT ABOVE THIS, EDIT USING EXTENSION POPUP

let products_clicked_on = 0;
let load_count = 0;
let already_loaded = [];

let regex = '';

let url = '';
let channel_name = '';

let chat_msg_id = '';
let product_title = '';
let product_price = '';
let stripped_price = '';
let within_price = false;
let is_wanted_product = '';
let amazon_link = '';
let wait_for_final_buy_button = 0;

function init() {
	url = window.location.href;
	
	if(url.includes('discord.com')) {
		// $('#chat-messages-808098582929735680 > div:nth-child(2) > div > div > div:nth-child(2)').textContent
		$("[id^=chat-messages-]").each(function (index) {
			chat_msg_id = $(this).attr('id');

			if(!already_loaded.includes(chat_msg_id)) {
                product_title = $('#' + chat_msg_id +' > div:nth-child(2) > div > div > div:nth-child(2)').text();

                // chat-messages-833867984611049562
                // chat_msg_id == 'chat-messages-833867984611049562' && // !already_loaded.includes(chat_msg_id) &&
                if (product_title.length > 0 && load_count > 2 && already_loaded.length >= 50) { // >= 50 important
                // if(chat_msg_id == 'chat-messages-833867984611049562' && !already_loaded.includes(chat_msg_id)) {
                    product_price = '';
                    if (channel_name === 'amazon-second') {
                        product_price = $('#' + chat_msg_id + ' > div:nth-child(2) > div > div > div:nth-child(3) > div:nth-child(2) > div:nth-child(2)').text();
                    } else {
                        //product_price = $('#chat-messages-822643746373632000 > div:nth-child(2) > div > div > div:nth-child(3) > div:nth-child(2) > div:nth-child(2)').text();
                        product_price = $('#' + chat_msg_id + ' > div:nth-child(2) > div > div > div:nth-child(3) > div:nth-child(2) > div:nth-child(2)').text();
                    }

                    stripped_price = Number.parseInt(product_price.substr(1).replace(/,/g, ''));
                    within_price =(stripped_price < max_price) || max_price == 0;
                    is_wanted_product = regex.test(product_title);

                    console.log('======================');
                    logData('New product: ' + product_title + ' ' + product_price);
                    logData(product_title + ' - Wanted Product: ' + is_wanted_product);
                    logData(stripped_price + ' - Within Price (' + max_price + '): ' + within_price);

                    if (is_wanted_product && within_price) {
                        logData('Buying: ' + product_title + ' ' + product_price + ' :: ' + chat_msg_id);

                        let href = $('#' + chat_msg_id +' > div:nth-child(2) > div > div > div:nth-child(2) > a')[0].href;
						let start = href.search('offer-listing/') + 'offer-listing/'.length;
						let end = href.search('ref') - 1;
						let sku = href.substring(start, end);

                        //window.open(href);

						amazon_link = '';
                        if (channel_name === 'amazon-second') {
                            amazon_link = $('#' + chat_msg_id + ' > div:nth-child(2) > div > div > div:nth-child(3) > div:nth-child(4) > div:nth-child(2) > a')[0];
                        } else {
                            // //*[@id="chat-messages-822643746373632000"]/div[2]/div/div/div[3]/div[5]/div[2]/a
                            // '#chat-messages-822643746373632000 > div:nth-child(2) > div > div > div:nth-child(3) > div:nth-child(5) > div:nth-child(2) > a'
                            amazon_link = $('#' + chat_msg_id + ' > div:nth-child(2) > div > div > div:nth-child(3) > div:nth-child(5) > div:nth-child(2) > a')[0];
                        }
                        amazon_link.click();

                        products_clicked_on++;
                    }

                    console.log('======================');
                }

                if (!already_loaded.includes(chat_msg_id)) {
                    already_loaded.push(chat_msg_id);
                }
            }
		});
	
        setTimeout(function() {
			load_count++;

			if(load_count % 50 === 0) {
                chrome.storage.local.set({'load_count': load_count});
                chrome.storage.local.set({'products_seen': already_loaded.length});
                chrome.storage.local.set({'products_clicked_on': products_clicked_on});

				logData('' + (delay_time/1000) + 's delay - Load count: ' + load_count + ' - Products seen: ' + already_loaded.length + ' - Products Clicked on: ' + products_clicked_on);
            }

            init();
        }, delay_time);
	} else if(url.includes('amazon.com')) {
		logData('On Amazon ...');

		// todo: make this wait until, instead of time delay
		// todo: make this part only work if name passes regex again


        if(wait_for_final_buy_button >= 1 && wait_for_final_buy_button < 10) { // todo: delete this block?
            if ($('#place-order-form').length > 0) {
                logData('Clicking FINAL!! buy now button');

                $('#place-order-form > div > span > span > span > input').click();
            }

            setTimeout(function () { // needed since next buy now button could be a popup
                logData('Waiting for popup buy now button (increase) ...')
                wait_for_final_buy_button++;
                init();
            }, 3000);
        } else {

            if (url.includes('yourstore')) {
                logData('Closing "your store"...');

                // chrome.runtime.sendMessage({closeThis: true}); // todo:
            } else if ($('h1.a-spacing-mini').length > 0 && $('h1.a-spacing-mini').text().includes('empty')) {
                logData('Closing window because cart is empty...');

                // chrome.runtime.sendMessage({closeThis: true}); // todo:
            } else if ($('[action="/gp/aws/cart/add.html"] > span').length > 0 && $('[action="/gp/aws/cart/add.html"] > span').text().includes('no items to add')) {
                logData('Closing window because there\'s no continue button...');

                // chrome.runtime.sendMessage({closeThis: true}); // todo:
            } else if ($('#buy-now-button').length > 0) {
                logData('Clicking buy now button');

                $('#buy-now-button').click()

                logData('Waiting for popup buy now button...')

                let intervalCount = 0;
                let waitforbuynowbutton = setInterval(function() {
                    logData('Running interval...')

                    let checkout_button = $('#turbo-checkout-iframe').contents().find('#turbo-checkout-pyo-button');

                    console.log(checkout_button);
                    if (checkout_button.length > 0) {
                        logData('Clicking FINAL!! buy now button');

                        //$('#place-order-form > div > span > span > span > input').click();

                        checkout_button.click();

                        //clearInterval(waitforbuynowbutton);
                    }

                    if(intervalCount > 30) {
                        clearInterval(waitforbuynowbutton);
                    }
                    intervalCount++;
                }, 2000);
            } else if ($('#buy-now-button').length > 0) {
                logData('Clicking turbo place your order button');

                $('#turbo-checkout-pyo-button').click();
            } else if ($('[name="placeYourOrder1"]').length > 0) {
                logData('Clicking place your order...');

                if (submit_order) {
                    logData('Clicking place your order...');
                    $('[name="placeYourOrder1"]').click();

                    setTimeout(function () {
                        // close window, only if we finally checkout after 10s
                        // chrome.runtime.sendMessage({closeThis: true}); // todo:
                    }, 10000);
                } else {
                    logData('Not clicking final place order, showing red square...');

                    var txt3 = document.createElement("div");  // Create with DOM
                    txt3.innerHTML = "Ready to order from discord bot.";
                    txt3.style.background = 'red';
                    txt3.style.position = 'absolute'
                    txt3.style.width = '100px'
                    txt3.style.height = '100px'
                    txt3.style.top = '0%'
                    txt3.style.left = '0%'
                    txt3.style.zIndex = '1000'

                    $('body').append(txt3);
                }
            } else if ($('[value="add"]').length > 0) {
                logData('Clicking add to cart button...');

                $('[value="add"]').click();

                setTimeout(function () {
                    logData('Waiting for place your order to appear...')
                    //init();
                }, 5000);
            } else if ($('[name="proceedToRetailCheckout"]').length > 0) {
                logData('Clicking proceed to checkout button...');

                $('[name="proceedToRetailCheckout"]').click();

                setTimeout(function () {
                    logData('Waiting for place your order to appear...')
                    //init();
                }, 3000);
            } else if ($('[aria-labelledby="shipToThisAddressButton-announce"]').length > 0) {
                logData('Clicking bottom "use this address" button...');

                $('[aria-labelledby="shipToThisAddressButton-announce"]').click(); // todo: test

                setTimeout(function () {
                    logData('Waiting for place your order to appear...')
                    //init();
                }, 3000);
            } else if ($('#hlb-ptc-btn-native').length > 0) {
                logData('Clicking Proceed to checkout (1 item) button...');

                $('#hlb-ptc-btn-native').click(); // todo: test
            } else if ($('.a-color-error').length > 0) {
                logData('Oh no there was an error?');

                logData($('.a-color-error')[0].textContent);

                if($('[name="submit.addToCart"]').length > 0) {
                    logData('Clicking on atc button.');
                    $('[name="submit.addToCart"]').click();
                } else if ($('.a-color-error')[0].textContent.includes('items in your order') || $('.a-color-error')[0].textContent.includes('problem')) {
                    logData('Closing window');
                    //$('[data-action="itemselect-delete-item-row"]').click();
                    // chrome.runtime.sendMessage({closeThis: true}); // todo:
                }
            } else if ($('#widget-purchaseConfirmationStatus').length > 0) {
                logData('Yay we checked out!');
            } else {
                setTimeout(function () {
                    logData('Waiting for next button to appear...')
                    init();
                }, 1000);
            }
        }
	}
}

function logData(msg) {
	var d = new Date();

    console.log('[DISCORD_RESELL - ' + d.toLocaleString() + '] - ' + msg);
}

let delay = window.location.href.includes('discord.com') ? 10000 : 0; // 10s on discord, 0s on amazon // todo:
let allowed_channels = ['amazon', 'amazon-second'];

setTimeout(function() {
    logData('Loading Discord Resell Extension...');
    chrome.storage.local.get(['run', 'max_price', 'submit_order', 'delay_time', 'wanted_products'], function(result) {
        console.log('===============');
        console.log("Discord (discord-content.js)");
        console.log('===============');

        if(window.location.href.includes('discord.com')) {
            channel_name = $("[class^=title-] > div > h3").text();

			logData('On discord.com - Channel: ' + channel_name);
        }

        wanted_products = result.wanted_products;
        max_price = result.max_price;
        submit_order = result.submit_order;
        delay_time = result.delay_time;

        wanted_products = wanted_products.split('|');
        wanted_products = wanted_products.join('|');

        regex = new RegExp(wanted_products, 'i');
        regex.ignoreCase;

        if((result.run == true || Object.keys(result).length == 0) && (window.location.href.includes('amazon.com') || allowed_channels.includes(channel_name))) {
            init();
        } else {
            logData('Run not enabled, or not in right channel.');
        }
    });
}, delay);