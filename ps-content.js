function init() {
	let queueWhatIsThis = document.getElementById('whatisthis');
	let url = window.location.href;

    //todo: TEST tell the difference between a captcha wait page and the captcha to enter the queue

	if(url.includes('softblock')) {
        logData('We\'ve been soft blocked. Waiting for extension to take care of it.');
        // id = lbHeaderP
        // We're experiencing very high traffic. You will be let into our store shortly.

        let rand = Math.floor(Math.random() * (60000 - 30000 + 1) + 60000); // 30-60s

        logData('Waiting: ' + Math.round(rand / 1000) + 's | ' + ((rand / 1000) / 60).toFixed(2) + ' min'); //  + rand + 'ms'

        setTimeout(function() {
            logData('Reloading function...')
            init();
        }, rand);
	} else if (typeof(queueWhatIsThis) != 'undefined' && queueWhatIsThis != null) {
        logData('We\'re on the real queue disabling run.');
		
		chrome.storage.local.set({'run': false});
		
		// todo: we shouldn't stop here, should send mins left...
	} else {
        // out-stock-wrpr

		let rand = Math.floor(Math.random() * (400000 - 60000 + 1) + 60000); // 60-400s

        logData('Waiting: ' + Math.round(rand / 1000) + 's | ' + ((rand / 1000) / 60).toFixed(2) + ' min'); //  + rand + 'ms'

		setTimeout(function() {
            logData('Reloading...')
			location.reload(true);
		}, rand);
	}
}

function logData(msg) {
	console.log(msg);

	$.post('https://resell.clients.easydeck.net/api/ping', {message: msg, url: window.location.href, extension_version: chrome.runtime.getManifest().version}, function(msg) {
		if(msg.stop) {
			console.log('Stopped running from server command...');

            chrome.storage.local.set({'run': false});
        }

        if(msg.pause.active) {
			console.log('Pausing running from server command. (' + msg.pause.time + ')')
		}
	});
}

chrome.storage.local.get(['run'], function(result) {
	console.log('===============');
    console.log("PlayStation Direct (ps-content.js)");
    console.log('===============');

  	if(result.run == true || Object.keys(result).length == 0) {
        init();
  	} else {
        logData('Run not enabled.');
  	}
});