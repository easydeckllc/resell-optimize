let delay = 3000;
let should_add_to_cart = false;

function init() {
    // let add_to_cart = $('.add-to-cart-button');
    let add_to_cart = $('.fulfillment-add-to-cart-button');

    if(add_to_cart.length > 1) {
        add_to_cart = add_to_cart[0];
    }

    if(add_to_cart.textContent.includes("Add to Cart")) {
    	logData('Button says add to cart!');

    	if($('#addtocart-alert').length == 0) {
    	    alert('Button says add to cart!');

            var txt3 = document.createElement("div"); // Create with DOM
            txt3.innerHTML = "Button is active!";
            txt3.id = 'addtocart-alert';
            txt3.style.background = 'red';
            txt3.style.position = 'fixed'
            txt3.style.width = '100px'
            txt3.style.height = '100px'
            txt3.style.top = '0%'
            txt3.style.left = '0%'
            txt3.style.zIndex = '1000'

            $('body').append(txt3);

            if (should_add_to_cart) {
                logData('Clicking add to cart');
                add_to_cart.click();
            }
        }

        setTimeout(function() {
            init();
        }, delay);
	} else {
        $('#addtocart-alert').remove();

    	logData('No add to cart button yet, waiting ' + delay + 'ms...');

        setTimeout(function() {
            init();
        }, delay);
	}
}

function logData(msg) {
    var d = new Date();

    console.log('[BESTBUY_RESELL - ' + d.toLocaleString() + ')] - ' + msg);
}

function reload(){
    window.location = 'https://api.bestbuy.com/click/-/6452940/cart?hmt=add';
}

function getRndInteger(min, max) {
    return Math.floor(Math.random() * (max - min) ) + min;
}

setTimeout(function() {
	chrome.storage.local.get(['run', 'bb_delay', 'bb_click_add_to_cart'], function(result) {
		console.log('===============');
		console.log("Bestbuy (bestbuy-content.js)");
		console.log('===============');

		if(result.bb_delay) {
			delay = result.bb_delay;
        }
        if(result.bb_click_add_to_cart) {
            should_add_to_cart = true;
        }

		if(result.run == true || Object.keys(result).length == 0) {
			init();
		} else {
			logData('Run not enabled.');
		}
	});
}, 10000); // 10s