function enable() {
	chrome.storage.local.set({'run': true});

	if(site == "discord") {
        chrome.storage.local.set({'wanted_products': $('#wanted_products').val()});
        chrome.storage.local.set({'max_price': $('#max_price').val()});
        chrome.storage.local.set({'delay_time': $('#delay_time').val()});
        if ($('#submit_order').prop("checked") == true) {
            chrome.storage.local.set({'submit_order': $('#submit_order').val()});
        } else {
            chrome.storage.local.set({'submit_order': 0});
        }
    } else if(site == "bestbuy") {
        chrome.storage.local.set({'bb_delay': $('#bb_delay').val()});
        if ($('#bb_click_add_to_cart').prop("checked") == true) {
            chrome.storage.local.set({'bb_click_add_to_cart': 1});
        } else {
            chrome.storage.local.set({'bb_click_add_to_cart': 0});
        }
    } else if(site == "telfar") {
        chrome.storage.local.set({'telfar_id': $('#telfar_id').val()});
        if ($('#telfar_final_checkout').prop("checked") == true) {
            chrome.storage.local.set({'telfar_final_checkout': $('#telfar_final_checkout').val()});
        } else {
            chrome.storage.local.set({'telfar_final_checkout': 0});
        }
    }

    updateStatus(true);

    logData('Started Run');
}

function stop() {
	chrome.storage.local.set({'run': false});

    updateStatus(false);

    logData('Stopped Run');
}

let site = '';

$(document).ready(function () {
    document.getElementById("enable-run").onclick = function() {
		enable();
	};

    document.getElementById("stop-run").onclick = function() {
		stop();
	};

    $('#version').text('v' + chrome.runtime.getManifest().version);

    chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
        var tab = tabs[0];
        var url = tab.url;

        if(url.includes('discord.com') || url.includes('amazon.com')) {
            site = 'discord';
            $('#discord_popup').show();

            chrome.storage.local.get(['run', 'max_price', 'submit_order', 'delay_time', 'wanted_products', 'load_count', 'products_seen', 'products_clicked_on'], function(result) {
                updateStatus(result.run);

                // load_count
                // products_seen
                // products_clicked_on

                $('#discord-status').html('Loop count <strong>' + result.load_count + '</strong> | Products seen <strong>' + result.products_seen + '</strong> | Products clicked on <strong>' + result.products_clicked_on + '</strong>');

                $('#wanted_products').val(result.wanted_products);
                $('#max_price').val(result.max_price);
                $('#delay_time').val(result.delay_time);
                if(result.submit_order == 1) {
                    $('#submit_order').attr('checked', true);
                }
            });
        } else if(url.includes('nike.com')) {
            site = 'nike';

            $('#nike_popup').show();

            chrome.storage.local.get(['run', 'ccv', 'sizes'], function(result) {
                updateStatus(result.run);

                $('#ccv').html('CCV: ' + result.ccv);
                $('#sizes').html('Sizes: ' + result.sizes.join(', '));
            });
        } else if(url.includes('bestbuy.com')) {
            site = 'bestbuy';

            $('#bestbuy_popup').show();

            chrome.storage.local.get(['run', 'bb_delay', 'bb_click_add_to_cart'], function(result) {
                updateStatus(result.run);

                $('#bb_delay').val(result.bb_delay);
                if(result.bb_click_add_to_cart == 1) {
                    $('#bb_click_add_to_cart').attr('checked', true);
                }
            });
        } else if(url.includes('telfar.net')) {
            site = 'telfar';

            $('#telfar_popup').show();

            chrome.storage.local.get(['run', 'telfar_id', 'telfar_final_checkout'], function(result) {
                updateStatus(result.run);

                $('#telfar_id').val(result.telfar_id);
                if(result.telfar_final_checkout == 1) {
                    $('#telfar_final_checkout').attr('checked', true);
                }
            });
        }
    });
});

function updateStatus(running) {
    if(running) {
        $('#run-status').html('<font color="green"><strong>ENABLED</strong></font>');
    } else {
        $('#run-status').html('<font color="red"><strong>DISABLED</strong></font>');
    }
}

function logData(msg) {
    console.log(msg);

    //$.post('https://resell.clients.easydeck.net/api/ping', {message: msg, url: window.location.href, extension_version: chrome.runtime.getManifest().version}, function() {
        // console.log('sent...')
    // });
}