function startATCClicking() {
    console.log('starting..');

    var handle = window.setInterval(function(){
        console.log('clicking atc, and undisable...');

        let button = $('.add-to-cart');
        button.attr('disabled', false);
        button.click();
    }, 45000);
}

$(function() {
    chrome.storage.local.get(['run'], function(result) {
        console.log('===============');
        console.log("GME (gme-content.js)");
        console.log('===============');

        if(result.run == true || Object.keys(result).length == 0) {
            // startATCClicking();
        }
    });
})