function startATCClicking() {
    setTimeout(function() {
        let button = $('[data-tl-id="ProductPrimaryCTA-cta_add_to_cart_button"]');
        button.click();

        setTimeout(function() {
            location.reload(true);
        }, 3000);
    }, 1500);

    /* var handle = window.setInterval(function(){
        let button = $('[data-tl-id="ProductPrimaryCTA-cta_add_to_cart_button"]').attr('disabled', false);
        button.click();
    }, 2000); */
}

$(function() {
    chrome.storage.local.get(['run'], function(result) {
        console.log('===============');
        console.log("Walmart (walmart-content.js)");
        console.log('===============');

        if(result.run == true || Object.keys(result).length == 0) {
           // startATCClicking();
        }
    });
})